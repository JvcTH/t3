﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.DB;
using T3DIARS.Models;

using Microsoft.EntityFrameworkCore;

namespace T3DIARS.Controllers
{
    public class RutinaController : Controller
    {
        private AppT3DIARSContext context;
        private Tipo tipo;
        public RutinaController(AppT3DIARSContext context)
        {
            this.context = context;
        }
        public ActionResult Index()
        {
            var usuario = GetUsuario();
            var rutinas = context.Rutinas.Where(o => o.UsuarioId == usuario.Id);
            return View(rutinas);
        }

        public ActionResult Ejercicios(int id)
        {
            var usuario = GetUsuario();
            var ejercicioRutinas = context.EjercicioRutinas.Where(o => o.RutinaId == id).Include(o => o.Ejercicio).ToList(); 

            return View(ejercicioRutinas);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.usuario = GetUsuario();
            return View(new Rutina());
        }

        [HttpPost]
        public ActionResult Create(Rutina rutina)
        {
            if (ModelState.IsValid)
            {
                if (rutina.Tipo == "Principiante") {
                    tipo = new Principiante();
                }
                if (rutina.Tipo == "Intermedio")
                {
                    tipo = new Intermedio();
                }
                if (rutina.Tipo == "Avanzado")
                {
                    tipo = new Avanzado();
                }
                
                var ejercicios = tipo.Ejercicios(context);
                
                context.Rutinas.Add(rutina);
                context.SaveChanges();

                var rutinas = context.Rutinas.ToList();
                var rutinaCreada = rutinas.LastOrDefault();

                Random random = new Random();
                
                foreach (var ejercicio in ejercicios) {
                    if (rutinaCreada.Tipo != "Avanzado")
                    {
                        context.EjercicioRutinas.Add(new RutinaEjercicio() { RutinaId = rutinaCreada.Id, EjercicioId = ejercicio.Id, Tiempo = random.Next(60,120) });
                        context.SaveChanges();
                    }
                    else {
                        context.EjercicioRutinas.Add(new RutinaEjercicio() { RutinaId = rutinaCreada.Id, EjercicioId = ejercicio.Id, Tiempo = 120 });
                        context.SaveChanges();
                    }
                    
                }


                return RedirectToAction("Index");
            }

            return View("Create", rutina);
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }

            return null;
        }

    }
}
