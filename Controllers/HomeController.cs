﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.DB;
using T3DIARS.Models;

namespace T3DIARS.Controllers
{
    public class HomeController : Controller
    {
        private AppT3DIARSContext context;
        public HomeController(AppT3DIARSContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var ejercicios = context.Ejercicios.ToList();

            return View(ejercicios);
        }
        public IActionResult Detalle(int id)
        {
            var ejercicio = context.Ejercicios.Where(o=>o.Id==id).FirstOrDefault();

            return View(ejercicio);
        }

    }
}
