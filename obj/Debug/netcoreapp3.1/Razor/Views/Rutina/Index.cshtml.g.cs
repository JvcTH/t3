#pragma checksum "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "c1035e301a00d40db815bbd63291713d6caf0d4b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Rutina_Index), @"mvc.1.0.view", @"/Views/Rutina/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\_ViewImports.cshtml"
using T3DIARS;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\_ViewImports.cshtml"
using T3DIARS.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c1035e301a00d40db815bbd63291713d6caf0d4b", @"/Views/Rutina/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6d2084f602be9553302081235e330f226606d6fc", @"/Views/_ViewImports.cshtml")]
    public class Views_Rutina_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<a class=""btn btn-primary"" href=""/home/index"">Todos los Ejercicios</a>

<a class=""btn btn-primary"" href=""/rutina/create"">Nueva Rutina</a>

<a class=""btn btn-primary"" href=""/rutina/index"">Rutinas Creadas</a>

<a class=""btn btn-danger"" href=""/auth/logout"">Salir</a>

<br />
<table class=""table"">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>Tipo</th>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 23 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n\r\n                <td>\r\n                    ");
#nullable restore
#line 28 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml"
               Write(item.Nombre);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n\r\n                <td>\r\n                    ");
#nullable restore
#line 32 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml"
               Write(item.Tipo);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </td>\r\n                <td>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 830, "\"", 867, 2);
            WriteAttributeValue("", 837, "/rutina/ejercicios?id=", 837, 22, true);
#nullable restore
#line 35 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml"
WriteAttributeValue("", 859, item.Id, 859, 8, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">Ver Ejercicios </a>\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 38 "G:\UPN\Diseño y Arquitectura de Software\Diars 2\T3DIARS\T3DIARS\Views\Rutina\Index.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </tbody>\r\n</table>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
