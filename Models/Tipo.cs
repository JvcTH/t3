﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.DB;

namespace T3DIARS.Models
{
    public abstract class Tipo
    {
        
        public abstract List<Ejercicio> Ejercicios(AppT3DIARSContext context);
        
    }
}
