﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace T3DIARS.Models
{
    public class Rutina
    {
        public int Id { get; set; }


        public string Nombre { get; set; }

        public string Tipo { get; set; }
        public int UsuarioId { get; set; }

        public List<RutinaEjercicio> Ejercicios { get; set; }

    }
}
