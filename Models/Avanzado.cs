﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.DB;
using T3DIARS.Models;

namespace T3DIARS
{
    public class Avanzado : Tipo
    {

        public override List<Ejercicio> Ejercicios(AppT3DIARSContext context)
        {
            var ejercicios = context.Ejercicios.OrderBy(or => Guid.NewGuid()).ToList();
            var ejerciciosSeleccionados = new List<Ejercicio>();
            for (int i = 0; i < 15; i++)
            {
                ejerciciosSeleccionados.Add(ejercicios[i]);
            }
            return ejerciciosSeleccionados;
        }
    }
}
