﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.Models;

namespace T3DIARS.DB.Mapping
{
    public class RutinaMap : IEntityTypeConfiguration<Rutina>
    {
        public void Configure(EntityTypeBuilder<Rutina> builder)
        {
            builder.ToTable("Rutina");
            builder.HasKey(o => o.Id);

            builder.HasMany(o => o.Ejercicios)
               .WithOne()
               .HasForeignKey(o => o.RutinaId);
        }
    }
}
