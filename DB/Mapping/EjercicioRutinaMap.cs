﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.Models;

namespace T3DIARS.DB.Mapping
{
    public class EjercicioRutinaMap : IEntityTypeConfiguration<RutinaEjercicio>
    {
        public void Configure(EntityTypeBuilder<RutinaEjercicio> builder)
        {
            builder.ToTable("EjercicioRutina");
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Ejercicio).
                WithMany().
                HasForeignKey(o => o.EjercicioId);

        }
    }
}
