﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3DIARS.DB.Mapping;
using T3DIARS.Models;

namespace T3DIARS.DB
{
    public class AppT3DIARSContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ejercicio> Ejercicios { get; set; }
        public DbSet<Rutina> Rutinas{ get; set; }
        public DbSet<RutinaEjercicio> EjercicioRutinas{ get; set; }

        public AppT3DIARSContext(DbContextOptions<AppT3DIARSContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new EjercicioRutinaMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());


        }
    }
}
